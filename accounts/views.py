from django.shortcuts import render
from accounts.forms import Login_Form, SignUp_Form
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.contrib.auth.models import User


# Create your views here.
def User_Login(request):
    if request.method == "POST":
        form = Login_Form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = Login_Form()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def User_Logout(request):
    logout(request)
    return redirect("login")


def User_SignUp(request):
    if request.method == "POST":
        form = SignUp_Form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            if password == confirm:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUp_Form()
    context = {"form": form}
    return render(request, "registration/signup.html", context)
