from django.urls import path
from accounts.views import User_Login, User_Logout, User_SignUp


urlpatterns = [
    path("login/", User_Login, name="login"),
    path("logout/", User_Logout, name="logout"),
    path("signup/", User_SignUp, name="signup"),
]
